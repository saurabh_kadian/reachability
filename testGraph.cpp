#include <bits/stdc++.h>
#include <time.h>
#include <windows.h>
#define debug(v,len) for(long long int i=0;i<len;++i)cout<<v[i]<<" ";cout<<endl;
#define debugMatrix(name,row,col) for(long long int i=0;i<row;++i){for(long long int j=0;j<col;++j)cout<<name[i][j]<<" ";cout<<endl;}
#define print(v) cout<<v<<endl;
#define gcd(v,h) __gcd(v,h)
#define popcount(v) __builtin_popcount(v)
#define sl(T) scanf("%lld",&T)
using namespace std;
typedef long long ll;
typedef vector<ll> vll;

class Graph
{
	ll row = 0, col = 0, depth = 0;
	vector<vector<vector<int> > > horizontal;														// For plane K, at index i(row) if there is a 1 at j it means an edge from j to j+1
	vector<vector<vector<int> > > vertical;										 					// For plane K, at index j(col) if there is a 1 at i it means an edge from i to i+1
	vector<vector<pair<ll, ll> > > outOfThePlane;													// Contains pairs(i,j) at index k for telling edges coming out of (i,j)
	vector<ll> values;
	public:
		Graph(ll allSame){
			row = col = depth = allSame;
			horizontal.resize(depth, vector<vector<int> >(row, vector<int>(col, 0)));
			vertical.resize(depth, vector<vector<int> >(col, vector<int>(row, 0)));
			outOfThePlane.resize(depth, vector<pair<ll,ll> >());
			values.clear();
		}

		Graph(ll localRow, ll localcol, ll localDepth){
			row = localRow;
			col = localcol;
			depth = localDepth;
			horizontal.resize(depth, vector<vector<int> >(row, vector<int>(col, 0)));
			vertical.resize(depth, vector<vector<int> >(col, vector<int>(row, 0)));
			outOfThePlane.resize(depth, vector<pair<ll,ll> >());
			values.clear();
		}

		~Graph(){
			// Find a better way to clean up
			horizontal.clear();
			vertical.clear();
			outOfThePlane.clear();
			values.clear();
		}

		bool populate(ll seed, double percentDensityPlane = 0.9, double percentDensityShift = 0.9){
			// Populate the graph with edges based on the random density factor choosen
			// percentDensityPlane = 0.9 by default which means there will be
			// 90% edges in every plane. The same goes for percentDensityShift
			// which is the percent of polls having edges moving from one plant to another
			srand(seed);
			// Populate every plane
			for(ll i = 0;i < depth;++i){
				ll edgesGenerated = 0;
				ll edgesToBeGenerated = row * col * percentDensityPlane;
				while(edgesGenerated < edgesToBeGenerated*2){
					ll generatedX = rand() % row;
					ll generatedY = rand() % col;
					bool horizontalEdge = rand() % 2;
					if(horizontalEdge && horizontal[i][generatedX][generatedY] == 0)
						edgesGenerated++, horizontal[i][generatedX][generatedY] = 1;
					else if(vertical[i][generatedY][generatedX] == 0)
						edgesGenerated++, vertical[i][generatedY][generatedX] = 1;
				}
				// for(int ii = 0;ii < row;++ii)
				// 	for(int j = 0;j < col;++j)
				// 		cout << horizontal[i][ii][j] << " ";
			}

			// Populate the 'z' edges
			for(ll i = 0;i < depth - 1;++i){
				ll edgesGenerated = 0;
				ll edgesToBeGenerated = row * col * percentDensityShift;
				while(edgesGenerated < edgesToBeGenerated){
					ll generatedX = rand() % row;
					ll generatedY = rand() % col;
					if(find(outOfThePlane[i].begin(), outOfThePlane[i].end(), make_pair(generatedX, generatedY)) == outOfThePlane[i].end())
						edgesGenerated++, outOfThePlane[i].push_back(make_pair(generatedX, generatedY));
				}
				sort(outOfThePlane[i].begin(), outOfThePlane[i].end());
			}

			return true;
		}

		bool isReachable(){																				// Check if (row, col, depth) is reachable from (0, 0, 0). Checks validity.
			// Start from (0, 0, 0)
			// And reach (row, col, depth)
			bool visit[row][col][depth];
			for(ll i = 0;i < row;++i)
				for(ll j = 0;j < col;++j)
					for(ll k = 0;k < depth;++k)
						visit[i][j][k] = false;
			queue<pair<ll, pair<ll,ll> > > Q;
			Q.push(make_pair(0, make_pair(0, 0)));
			while(!Q.empty()){
				ll x = Q.front().first;
				ll y = Q.front().second.first;
				ll z = Q.front().second.second;
				// cout << x << ", " << y << ", " << z << endl;
				if(Q.front() == make_pair(row - 1, make_pair(col - 1, depth - 1)))
					return true;
				if(x + 1 >= row || y + 1 >= col || z + 1 >= depth){
					Q.pop();
					continue;
				}
				if(find(outOfThePlane[z].begin(), outOfThePlane[z].end(), make_pair(x, y)) != outOfThePlane[z].end() && !visit[x][y][z + 1])
					Q.push(make_pair(x, make_pair(y, z + 1))), visit[x][y][z + 1] = true;
				if(horizontal[z][x][y] == 1 && !visit[x][y + 1][z])
					Q.push(make_pair(x, make_pair(y + 1, z))), visit[x][y + 1][z] = true;
				if(vertical[z][y][x] == 1 && !visit[x + 1][y][z])
					Q.push(make_pair(x + 1, make_pair(y, z))), visit[x + 1][y][z] = true;
				Q.pop();
			}
			return false;
		}

		bool generate(ll seed, double percentDensityPlane = 0.9, double percentDensityShift = 0.9){
			populate(seed, percentDensityPlane, percentDensityShift);
			// int c = 0;
			// if(!isReachable()){
			// 	populate(seed * rand() % 10, percentDensityPlane, percentDensityShift);
			// 	c++;
			// 	cout << c << endl;
			// 	if(c > 100){
			// 		return false;
			// 	}
			// }
			return true;
		}
		ll recurse(ll x, ll y, ll z, ll w){
		    if(x >= row || y >= col || z >= depth)
                return 0;
		    // cout << x << ", " << y << ", " << z << ", " << w << endl;
			if(x == row-1 && y == col-1 && z == depth-1){
				values.push_back(w);
				return w;
			}
			if(find(outOfThePlane[z].begin(), outOfThePlane[z].end(), make_pair(x, y)) != outOfThePlane[z].end())
				recurse(x, y, z+1, powl(row, 8)*(x+1)*(y+1) + w);
			if(horizontal[z][x][y] && y < col)
				recurse(x, y + 1, z, powl(row, 4)*(x + 1) + w);
			if(vertical[z][y][x] && x < row)
				recurse(x + 1, y, z, 1 + y + w);
			return 0;
		}
		void rec(){
			recurse(0, 0, 0, 0);
		}
		// Find all the possible paths to reach (row, col, depth) and return their weights
		vector<pair<ll, string> > weights(){
						/*

							Set weighting scheme here
								Initial weighting scheme is
									if horizontal taken + i
									if vertical take    + j*n^4
									if changed plane    + i*j*n^8
								where n is the depth of the graph

						**/
			vector<pair<ll, string> > values;
			queue< pair< pair<pair<ll,ll>, pair<ll,ll> >, string> > Q;
			Q.push(make_pair(make_pair(make_pair(0, 0), make_pair(0, 0)), ""));
			while(!Q.empty()){
				// cout << Q.size() << endl;
				ll x = Q.front().first.first.first;
				ll y = Q.front().first.first.second;
				ll z = Q.front().first.second.first;
				ll w = Q.front().first.second.second;
				string path = Q.front().second;
				if(x >= row || y >= col || z >= depth){
					Q.pop();
					continue;
				}
				// cout << x << ", " << y << ", " << z << endl;
				if(x == row - 1 && y == col - 1 && z == depth - 1){
					values.push_back(make_pair(w, path));
                    Q.pop();
					continue;
				}
				if(find(outOfThePlane[z].begin(), outOfThePlane[z].end(), make_pair(x, y)) != outOfThePlane[z].end())
					Q.push(make_pair(make_pair(make_pair(x, y), make_pair(z + 1, (powl(row, 8) * (1 + x) * (1 + y)) + w)), path + "U"));
				if(horizontal[z][x][y] == 1)
					Q.push(make_pair(make_pair(make_pair(x, y + 1), make_pair(z, powl(row, 4)*(1 + x) + w)), path + "H"));
				if(vertical[z][y][x] == 1)
					Q.push(make_pair(make_pair(make_pair(x + 1, y), make_pair(z, w /*+ y + 1*/)), path + "V"));
				Q.pop();
			}
			// cout << values.size() << endl;
			sort(values.begin(), values.end());
			return values;

		}
    /*
		bool isMinUnique(){																				// Check if the graph is minUnique under the current weighting scheme
			// isReachable();
			vector<ll> values = weights();
			for(ll i = 0;i < values.size();++i)
				cout << values[i] << endl;
			cout << values[0] << " " << values[1] << endl;
			sort(values.begin(), values.end());
			return !(values[0] == values[1]);
		}
		*/

};

int main(){
    // freopen("outputGraph.txt", "w", stdout);
	// Graph graph(5, 5, 2);
	// cout << (graph.generate(time(NULL), 1, 1) ? (graph.isMinUnique() ? "The graph is min unique" : "The graph is not min unique") : "Not Reach" ) << endl;
	for(int i = 0;i < 1;++i){
        Sleep(1000);
        bool flag = false;
        while(!flag){
            Graph graph(10, 10, 10);
            // graph.generate(time(NULL), 0.8, 0.8);
            graph.generate(time(NULL), 1, 1);

            vector<pair<ll, string> > w = graph.weights();

            for(int i = 0;i < w.size();++i){
            	cout << w[i].first << " -- " << w[i].second <<endl;
            }
            return 0;

/*
            if(w.size() < 1)
                Sleep(1);//cout << "Not Reach" << endl;
            else{
                flag = true;
                cout << w[0] << " -> ";
                cout << (w[0]/((ll)powl(4, 8))) << "*n^8 + " << ((w[0]%((ll)powl(4, 8)))/(ll)powl(4, 4)) << "*n^4 + " << ((w[0]%(ll)powl(4, 8))%(ll)powl(4, 4)) << endl;
                if(w.size() > 1){
                    cout << w[1] << " -> ";
                    cout << (w[1]/((ll)powl(4, 8))) << "*n^8 + " << ((w[1]%((ll)powl(4, 8)))/(ll)powl(4, 4)) << "*n^4 + " << ((w[1]%(ll)powl(4, 8))%(ll)powl(4, 4)) << endl;
                }

                cout << ((w[0] != w[1]) ? "minUnique" : "XXXXXXXXXXXXXXXXXnotMinUniqueXXXXXXXXXXXXXXXX") << endl;
            }

            // cout << (graph.isReachable() ? "Reach" : "Not Reach") << endl;
        }
        */
	}
}
	return 0;
}
