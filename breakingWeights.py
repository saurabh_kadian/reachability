from collections import defaultdict, Counter, Iterable
from functools import lru_cache
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.mplot3d import Axes3D
from pprint import pprint as pp
import itertools
import matplotlib as mpl
import random

class supercomplex(tuple):
	def __add__(self, other):
		return supercomplex(tuple(map(sum, itertools.zip_longest(self, other, fillvalue=0))))

class Grid:
	def __init__(self, weightScheme, size, origin=(1,1,1), p=1):
		self.size = size
		self.origin = origin
		self.p = p
		self.endpoint = self.size, self.size, self.size
		self.weightScheme = weightScheme

		def withinBoundary(point):
			x, y, z = point
			a, b, c = self.endpoint
			return (1 <= x <= a and 1 <= y <= b and 1 <= z <= c)

		def possibleNeighbors(point):
			x, y, z = point
			return filter(withinBoundary, [(x+1, y, z), (x, y+1, z), (x, y, z+1)])

		self.edges = {}
		for x in range(1, self.size+1):
			for y in range(1, self.size+1):
				for z in range(1, self.size+1):
					point = x, y, z
					self.edges[point] = [neigh for neigh in possibleNeighbors(point) if random.random() <= self.p]
		self.edges[self.endpoint] = []

		@lru_cache(None)
		def makePaths(p1):
			if p1 == self.endpoint:
				return [[p1]]

			paths = []
			for p2 in self.edges[p1]:
				paths.extend(makePaths(p2))

			return [[p1] + path for path in paths]

		self.allPaths = makePaths(self.origin)
		self.pathWeights = list(map(self.pathWeight, self.allPaths))

	def pathWeight(self, path):
		horizontal, vertical, outofplane = 0, 1, 2
		def direction(x1, y1, z1, x2, y2, z2):
			return (y2-y1)*1 + (z2-z1)*2

		weight = supercomplex([0])
		for p1, p2 in zip(path, path[1:]):
			direc = direction(*p1, *p2)
			w = self.weightScheme(p1, direc, self.size)
			if not isinstance(w, Iterable):
				w = [w]
			weight += supercomplex(w)
		return weight


	def checkIntersection(self, p1, p2):
		# check if the paths intersect

		pointCounts = Counter(p1[1:-1]) + Counter(p2[1:-1])
		for c in pointCounts.values():
			if c > 1: return True
		return False


		last = 0
		for cur, (a, b) in enumerate(zip(p1, p2)):
			if a == b:
				x = p1[last:cur+1]
				y = p2[last:cur+1]
				if self.pathWeight(x) != self.pathWeight(y):
					return True
				last = cur
		return False

@lru_cache(None)
def factorial(n):
	assert n >= 0
	if n <= 1: return 1
	return n * factorial(n-1)

def formulaPaths(size):
	return factorial(3*size) // (factorial(size)**3)

def verifyPathCount():
	# Verifies that the number of paths found in a complete cube of size matches the one amount given by the formula
	for size in range(2, 7):
		print(formulaPaths(size-1), len(Grid(weightScheme=lambda *args: 0, size=size, p=1).allPaths))

def verifySchemeManually(scheme):
	# For the specified size, outputs the weights of each path, so that the weighting scheme correctness can be verified manually
	# (doesn't check if the weighting scheme is good, just that it is implemented correctly)
	size = 2
	grid = Grid(weightScheme=scheme, size=size, p=1)
	for path, weight in zip(grid.allPaths, grid.pathWeights):
		print(path, weight)

# def checkIntersection(p1, p2):
# 	# check if the paths intersect
# 	pointCounts = Counter(p1[1:-1]) + Counter(p2[1:-1])
# 	for c in pointCounts.values():
# 		if c > 1: return True
# 	return False

def plotPaths(p1, p2, size, weight):
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	ax.plot(*zip(*p1), label='path 1')
	ax.plot(*zip(*p2), label='path 2')
	ax.legend()
	ax.xaxis.set_major_locator(MaxNLocator(integer=True))
	ax.yaxis.set_major_locator(MaxNLocator(integer=True))
	ax.zaxis.set_major_locator(MaxNLocator(integer=True))
	plt.title('Path Weight: {}, Cube Size: {}'.format(weight, size))
	plt.show()


def breakScheme(scheme, plot=False):
	# Returns the first non-intersecting, equal-weight (not necessarily min) pair of paths it can find for any complete grid
	for size in itertools.count(2):
		print('\nTesting grids of size:', size)
		grid = Grid(size=size, p=1, weightScheme=scheme)

		# check if duplicate path weights exist at all
		if len(grid.allPaths) == len(set(grid.pathWeights)):
			print('No duplicated weights')
			continue

		print('Duplicate path weights exist!')
		groups = defaultdict(list)
		for path, weight in zip(grid.allPaths, grid.pathWeights):
			groups[weight].append(path)

		for weight, group in groups.items():
			if len(group) > 1:
				for p1 in group:
					for p2 in group:
						if p1 == p2: continue
						# if not checkIntersection(p1, p2):
						if not grid.checkIntersection(p1, p2):
							print('\nFound two paths of weight {} in a complete cube of size {} that dont intersect!'.format(weight, size))
							print('Path 1:', p1)
							print('Path 2:', p2)
							plotPaths(p1, p2, size, weight)
							return p1, p2, size, weight
		print('All common-weight paths intersected. Moving to higher cube size')



horizontal, vertical, outofplane = 0, 1, 2


def weightScheme_extreme_maybeworks(point, direction, cubeSize):
	x, y, z = point
	if direction == horizontal:
		return x, y, z, x*y, y*z, x*z, x*y*z,         0, 0, 0, 0, 0, 0, 0,         0, 0, 0, 0, 0, 0, 0
	elif direction == vertical:
		return 0, 0, 0, 0, 0, 0, 0,           x, y, z, x*y, y*z, x*z, x*y*z,       0, 0, 0, 0, 0, 0, 0
	elif direction == outofplane:
		return 0, 0, 0, 0, 0, 0, 0,                    0, 0, 0, 0, 0, 0, 0,         x, y, z, x*y, y*z, x*z, x*y*z

def make_1hot(*val, pos):
	pad = [0] * len(val)
	return tuple(pad * pos + list(val) + pad * (3-pos-1))

def weightScheme_original(point, direction, cubeSize):
	# fails
	x, y, z = point
	if direction == horizontal:
		return 0
	elif direction == vertical:
		return x
	elif direction == outofplane:
		return (x*y) * 1j

def weightScheme_extreme(point, direction, cubeSize):
	x, y, z = point
	if direction == horizontal:
		return make_1hot(y, z, x*y*z,                                  pos=0)
	elif direction == vertical:
		return make_1hot(x, z, x*y*z,                                  pos=1)
	elif direction == outofplane:
		return make_1hot(x, y, x*y*z,                                  pos=2)

def weightScheme_1(point, direction, cubeSize):
	# fails
	x, y, z = point
	if direction == horizontal:
		return x, x*y*z,    0, 0,     0, 0
	elif direction == vertical:
		return 0, 0,      y, x*y*z,     0, 0
	elif direction == outofplane:
		return 0, 0,      0, 0,     z, x*y*z

def weightScheme_prime(point, direction, cubeSize):
	# fails
	x, y, z = point
	if direction == horizontal:
		return make_1hot(0, pos=0)
	elif direction == vertical:
		return make_1hot(x, pos=0)
	elif direction == outofplane:
		return make_1hot((5*x + 7*y)*z, pos=1)

if __name__ == '__main__':
	# verifyPathCount()
	# verifySchemeManually(weightScheme_1)
	p1, p2, size, weight = breakScheme(weightScheme_original, plot=True)