#include <bits/stdc++.h>
#include <time.h>
#include <windows.h>
#define debug(v,len) for(long long int i=0;i<len;++i)cout<<v[i]<<" ";cout<<endl;
#define debugMatrix(name,row,col) for(long long int i=0;i<row;++i){for(long long int j=0;j<col;++j)cout<<name[i][j]<<" ";cout<<endl;}
#define print(v) cout<<v<<endl;
#define gcd(v,h) __gcd(v,h)
#define popcount(v) __builtin_popcount(v)
#define sl(T) scanf("%lld",&T)
using namespace std;
typedef long long ll;

class Graph{
	ll row = 0, col = 0;
	vector<vector<int> > horizontal;
	vector<vector<int> > vertical;
	vector<ll> values;

	public:
		Graph(ll allSame){
			row = col = allSame;
			horizontal.resize(row, vector<int>(col, 0));
			vertical.resize(row, vector<int>(col, 0));
			values.clear();
		}
		Graph(ll localRow, ll localCol){
			row = localRow;
			col = localCol;
			horizontal.resize(row, vector<int>(col, 0));
			vertical.resize(row, vector<int>(col, 0));
			values.clear();
		}
		~Graph(){
			horizontal.clear();
			vertical.clear();
			values.clear();
		}

		bool populate(ll seed, double percentDensity = 0.9){
			srand(seed);

			// Populate
			ll edgesGenerated = 0;
			ll edgesToBeGenerated = row * col * percentDensity;
			while(edgesGenerated < edgesToBeGenerated*2){
				ll generatedX = rand() % row;
				ll generatedY = rand() % col;
				bool horizontalEdge = rand() % 2;
				if(horizontalEdge && horizontal[generatedX][generatedY] == 0)
					edgesGenerated++, horizontal[generatedX][generatedY] = 1;
				else if(vertical[generatedY][generatedX] == 0)
					edgesGenerated++, vertical[generatedY][generatedX] = 1;
			}

			return true;
		}

		bool generate(ll seed, double percentDensity = 0.9){
			populate(seed, percentDensity);
			return true;
		}

		vector<pair<ll, string> > weights(){
			vector<pair<ll, string> > values;
			queue<pair<pair<ll, ll>, pair<ll, string> > > Q;
			Q.push(make_pair(make_pair(0, 0), make_pair(0, "")));
			while(!Q.empty()){
				ll x = Q.front().first.first;
				ll y = Q.front().first.second;
				ll w = Q.front().second.first;
				string path = Q.front().second.second;
				if(x >= row || y >= col){
					Q.pop();
					continue;
				}
				if(x == row - 1 && y == col-1){
					values.push_back(make_pair(w, path));
					Q.pop();
					continue;
				}
				if(horizontal[x][y] == 1)
					Q.push(make_pair(make_pair(x, y+1), make_pair(w + (x + 1) + (y + 1)*powl(row, 4) + (x+1)*(y+1)*powl(row, 8), path + "H")));
				if(vertical[x][y] == 1)
					Q.push(make_pair(make_pair(x+1, y), make_pair(w + (x+1)*powl(row, 12) + (y+1)*powl(row, 16) + (x+1)*(y+1)*powl(row, 20), path + "V")));
				Q.pop();
			}
			sort(values.begin(), values.end());
			return values;
		}

};

int main(){
	for(int i = 0;i < 100;++i){
		Sleep(100);
		bool flag = false;
		while(!flag){
			Graph graph(4, 4);
			graph.generate(time(NULL), 0.9);
			vector<pair<ll, string> > w = graph.weights();

			if(w.size() < 1)
                Sleep(1);
            else{
                flag = true;
                cout << w[0].first << " -> " << w[0].second << endl;
                if(w.size() > 1){
                    cout << w[1].first << " -> " << w[1].second << endl;
                }
                cout << ((w[0].first != w[1].first) ? "minUnique" : "XXXXXXXXXXXXXXXXXnotMinUniqueXXXXXXXXXXXXXXXX") << endl;
            }

		}
	}
	return 0;
}
